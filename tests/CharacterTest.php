<?php

use PHPUnit\Framework\TestCase;
use App\Factory\CharacterFactory;

class CharacterTest extends TestCase
{
    public function testHeroStats()
    {

        $name = 'Orderus';

        $hero = CharacterFactory::hero($name);

        $this->assertEquals($name, $hero->name);

        $this->assertTrue(70 <= $hero->health && $hero->health <= 100);
        $this->assertTrue(70 <= $hero->strength && $hero->strength <= 80);
        $this->assertTrue(45 <= $hero->defence && $hero->defence <= 55);
        $this->assertTrue(40 <= $hero->speed && $hero->speed <= 50);
        $this->assertTrue(10 <= $hero->luck && $hero->luck <= 30);

    }

    public function testCharacterStats()
    {

        $name = 'Wild Beast';

        $character = CharacterFactory::character($name);

        $this->assertEquals($name, $character->name);

        $this->assertTrue(60 <= $character->health && $character->health <= 90);
        $this->assertTrue(60 <= $character->strength && $character->strength <= 90);
        $this->assertTrue(40 <= $character->defence && $character->defence <= 60);
        $this->assertTrue(40 <= $character->speed && $character->speed <= 60);
        $this->assertTrue(25 <= $character->luck && $character->luck <= 40);
    }

    public function testDamageTaken()
    {
        $character = CharacterFactory::character();
        $characterInitialHealth = $character->health;
        $damage = 20;

        $character->takeDamage($damage);

        $this->assertEquals($character->health, $characterInitialHealth - $damage);
    }

    public function testCharacterAttackWithNoLuck()
    {
        $hero = CharacterFactory::hero();
        $character = CharacterFactory::character();

        $character->luck = 0;
        $hero->RAPID_STRIKE_LUCK = 0;

        $characterInitialHealth = $character->health;

        $hero->attack($character);

        $damage = $hero->strength - $character->defence;

        $this->assertEquals($character->health, $characterInitialHealth - $damage);
    }


    public function testCharacterAttackWithLuck()
    {
        $hero = CharacterFactory::hero();
        $character = CharacterFactory::character();

        $character->luck = 100;
        $hero->RAPID_STRIKE_LUCK = 0;

        $characterInitialHealth = $character->health;

        $hero->attack($character);

        $this->assertEquals($character->health, $characterInitialHealth );
    }

    public function characterDefendWithLuck()
    {
        $character = CharacterFactory::character();

        $characterInitialHealth = $character->health;
        $character->luck = 100;

        $character->defend($damage = 20);

        $this->assertEquals($character->health, $characterInitialHealth);
    }

    public function characterDefendWithNoLuck()
    {
        $character = CharacterFactory::character();

        $characterInitialHealth = $character->health;
        $character->luck = 0;

        $character->defend($damage = 20);

        $this->assertEquals($character->health, $characterInitialHealth - $damage);
    }

    public function testRapidStrike()
    {
        $hero = CharacterFactory::hero();
        $character = CharacterFactory::character();

        $character->luck = 0;
        $hero->RAPID_STRIKE_LUCK = 100;

        $characterInitialHealth = $character->health;

        $hero->attack($character);

        $damage = $hero->strength - $character->defence;

        $this->assertEquals($character->health, $characterInitialHealth - $damage * 2);
    }

    public function testMagicShield()
    {
        $hero = CharacterFactory::hero();
        $character = CharacterFactory::character();

        $hero->luck = 0;
        $hero->MAGIC_SHIELD_LUCK = 100;

        $heroInitialHealth = $hero->health;

        $character->attack($hero);

        $damage = $character->strength - $hero->defence;

        $this->assertEquals($hero->health, $heroInitialHealth - $damage / 2);
    }

    public function testCharacterIsAlive()
    {
        $character = CharacterFactory::character();

        $damage = 20;
        $character->takeDamage($damage);

        $this->assertTrue($character->isAlive());
    }

    public function testCharacterIsDead()
    {
        $character = CharacterFactory::character();

        $damage = $character->health;
        $character->takeDamage($damage);

        $this->assertTrue($character->isDead());
    }
}