<?php


namespace App\Windows;


use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\ConsoleOutput;

class ConsoleWindow
{
    /**
     * @var ConsoleOutput
     */
    public $output;

    public function __construct()
    {
        $this->output = new ConsoleOutput();
    }

    public function showMessage(string $message)
    {
        $this->output->writeln($message);
        sleep(1);
    }

    public function showMessages(array $messages)
    {
        foreach ($messages as $message) {
            $this->showMessage($message);
        }
    }

    /**
     * Method that will output in the interface a table
     *
     * @param array $tableHeaders
     * @param array $rows
     */
    public function showTable(array $tableHeaders, array $rows)
    {
        (new Table($this->output))
            ->setHeaders($tableHeaders)
            ->setRows($rows)
            ->render();

        sleep(0.5);
    }

    public function separator()
    {
        $this->output->writeln('----------------------------------------');
    }
}