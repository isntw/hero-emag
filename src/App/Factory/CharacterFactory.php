<?php


namespace App\Factory;


use App\Model\Character;
use App\Model\Hero;

class CharacterFactory
{

    /**
     * Function that create an instance of Hero
     *
     * @param string $name
     * @return Hero
     */

    public static function hero(string $name = 'Orderus'): Hero
    {
        $character = new Hero($name);

        $character->health = rand(70, 100);
        $character->strength = rand(70, 80);
        $character->defence = rand(45, 55);
        $character->speed = rand(40, 50);
        $character->luck = rand(10, 30);

        return $character;
    }

    /**
     * Function that create an instance of Character
     *
     * @param string $name
     * @return Character
     */

    public static function character(string $name = 'Wild Beast'): Character
    {
        $character = new Character($name);

        $character->health = rand(60, 90);
        $character->strength = rand(60, 90);
        $character->defence = rand(40, 60);
        $character->speed = rand(40, 60);
        $character->luck = rand(25, 40);

        return $character;
    }

}