<?php


namespace App\Interfaces;


use App\Model\Character;

interface FightingInterface
{
    public function attack(Character $enemy): array;

    public function defend(int $damage): string;
}