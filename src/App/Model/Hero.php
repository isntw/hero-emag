<?php

namespace App\Model;

//● Health: 70 - 100
//● Strength: 70 - 80
//● Defence: 45 – 55
//● Speed: 40 – 50
//● Luck: 10% - 30% (0% means no luck, 100% lucky all the time).

use App\Interfaces\FightingInterface;

class Hero extends Character implements FightingInterface
{

    public $RAPID_STRIKE_LUCK = 10;
    public $MAGIC_SHIELD_LUCK = 20;


    public function rapidStrike()
    {
        return mt_rand(0, 100) < $this->RAPID_STRIKE_LUCK;
    }

    public function magicShield()
    {
        return mt_rand(0, 100) < $this->MAGIC_SHIELD_LUCK;
    }

    /**
     * Method that will attempt attack the enemy and will
     * output the outcome
     *
     * Damage = Attacker strength – Defender defence
     *
     * @param Character $enemy
     * @return string
     */
    public function attack(Character $enemy): array
    {
        $attackOutcome = [];

        $damage = $this->strength - $enemy->defence;

        if ($this->rapidStrike()) {
            $attackOutcome[] = "Rapid Strike Active";
            $damage *= 2;
        }

        $attackOutcome[] = $enemy->defend($damage);
        return $attackOutcome;
    }

    /**
     * Method that will attempt to defend the damage
     * received from an attack
     *
     * @param int $damage
     * @return string
     */
    public function defend(int $damage): string
    {
        if ($this->hasLuck()) {
            return "$this->name <fg=green>successfully</> defended the attack.";
        }

        if ($this->magicShield()) {
            $this->takeDamage($damage = $damage / 2);
            return "Magic Shield Active. $this->name took <error>$damage</error> damage.";
        }

        $this->takeDamage($damage);
        return "$this->name took <error>$damage</error> damage.";
    }

}
