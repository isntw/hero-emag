<?php

namespace App\Model;

use App\Interfaces\FightingInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * This is an interface so that the model is coupled to a specific backend.
 *
 * This is also so that we can demonstrate how to bind an interface
 * to an implementation with PHP-DI.
 */
class Character implements FightingInterface
{
    public $name;
    public $health;
    public $strength;
    public $defence;
    public $speed;
    public $luck;

    public $turn = false;

    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Method that will attempt attack the enemy and will
     * output the outcome
     *
     * Damage = Attacker strength – Defender defence
     *
     * @param Character $enemy
     * @return array
     */
    public function attack(Character $enemy): array
    {
        $damage = $this->strength - $enemy->defence;

        return [
            $enemy->defend($damage)
        ];
    }

    /**
     * Method that will attempt to defend the damage
     * received from an attack
     *
     * @param int $damage
     * @return string
     */
    public function defend(int $damage): string
    {
        if ($this->hasLuck()) {
            return "$this->name <fg=green>successfully</> defended the attack.";
        }

        $this->takeDamage($damage);
        return "$this->name took <error>$damage</error> damage.";
    }

    public function takeDamage($damage)
    {
        $this->health = $this->health - $damage;
    }

    public function hasLuck()
    {
        return mt_rand(0, 100) < $this->luck;
    }

    public function isDead()
    {
        return $this->health <= 0;
    }

    public function isAlive()
    {
        return $this->health > 0;
    }

    public function isFaster(Character $enemy)
    {
        return $this->speed > $enemy->speed;
    }

    public function isLuckier(Character $enemy)
    {
        return $this->luck > $enemy->luck;
    }

}
