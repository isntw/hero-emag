<?php


namespace App\Model;


use App\Windows\ConsoleWindow;

class Arena
{
    /**
     * @var ConsoleWindow
     */
    private $consoleWindow;

    /**
     * @var string
     */
    public $arenaName = 'Emagia';

    /**
     * @var Character
     */
    public $FIRST_FIGHTER;

    /**
     * @var Character
     */
    public $SECOND_FIGHTER;

    /**
     * @var int
     */
    public $round;

    /**
     * The Singleton's instance is stored in a static field. This field is an
     * array, because we'll allow our Singleton to have subclasses. Each item in
     * this array will be an instance of a specific Singleton's subclass. You'll
     * see how this works in a moment.
     */
    private static $instances = [];

    /**
     * The Singleton's constructor should always be private to prevent direct
     * construction calls with the `new` operator.
     */
    protected function __construct()
    {
        $this->consoleWindow = new ConsoleWindow();
    }

    /**
     * Singletons should not be cloneable.
     */
    protected function __clone()
    {
    }

    /**
     * Singletons should not be restorable from strings.
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    /**
     * This is the static method that controls the access to the singleton
     * instance. On the first run, it creates a singleton object and places it
     * into the static field. On subsequent runs, it returns the client existing
     * object stored in the static field.
     *
     * This implementation lets you subclass the Singleton class while keeping
     * just one instance of each subclass around.
     */
    public static function getInstance(): Arena
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static;
        }

        return self::$instances[$cls];
    }


    /**
     * Method that will start the match between two
     * characters
     *
     * @param $firstFighter
     * @param $secondFighter
     *
     * @return $this
     */
    public function start(Character $firstFighter, Character $secondFighter)
    {
        $this->FIRST_FIGHTER = $firstFighter;
        $this->SECOND_FIGHTER = $secondFighter;
        $this->round = 1;

        $this->consoleWindow->showMessages([
            "Game starting...",
            "{$this->FIRST_FIGHTER->name} enters {$this->arenaName}...",
            "As {$this->FIRST_FIGHTER->name} walks through {$this->arenaName} world he encounters {$this->SECOND_FIGHTER->name}."
        ]);

        $this->printCharactersStats();

        return $this;
    }

    /**
     * Method that will simulate the fight between
     * the characters
     *
     * @return $this
     */
    public function beginFight()
    {
        $this->consoleWindow->showMessage("Fight Begin.");

        while ($this->matchInProgress()) {
            $attacker = $this->getAttacker();

            $this->consoleWindow->showMessages([
                "Round $this->round.",
                "{$this->FIRST_FIGHTER->name} Health {$this->FIRST_FIGHTER->health}.",
                "{$this->SECOND_FIGHTER->name} Health {$this->SECOND_FIGHTER->health}."
            ]);

            $this->consoleWindow->showMessages(
                $attackOutcome = $attacker->attack(
                    $defender = $this->getDefender()
                )
            );

            $this->setupNextRound();
        }

        return $this;
    }

    /**
     * Method that will prepare the characters
     * for the next round
     */
    public function setupNextRound()
    {
        $this->consoleWindow->separator();

        $this->FIRST_FIGHTER->turn = !$this->FIRST_FIGHTER->turn;
        $this->SECOND_FIGHTER->turn = !$this->SECOND_FIGHTER->turn;


        $this->round += 1;
        sleep(1);
    }

    public function matchInProgress()
    {
        return $this->FIRST_FIGHTER->isAlive() && $this->SECOND_FIGHTER->isAlive() && $this->round <= 20;
    }

    /**
     * Method that will decide whom is the attacker
     *
     * @return Character
     */
    public function getAttacker(): Character
    {
        if ($this->round == 1) {
            
            if ($this->FIRST_FIGHTER->speed === $this->SECOND_FIGHTER->speed) {
                return $this->FIRST_FIGHTER->isLuckier($this->SECOND_FIGHTER)
                    ? $this->FIRST_FIGHTER
                    : $this->SECOND_FIGHTER;
            } else {
                return $this->FIRST_FIGHTER->isFaster($this->SECOND_FIGHTER)
                    ? $this->FIRST_FIGHTER
                    : $this->SECOND_FIGHTER;
            }

        } else {
            return $this->FIRST_FIGHTER->turn
                ? $this->FIRST_FIGHTER
                : $this->SECOND_FIGHTER;
        }
    }

    /**
     * Method that will decide whom is the defender
     *
     * @return Character
     */
    public function getDefender(): Character
    {
        return $this->getAttacker() == $this->FIRST_FIGHTER
            ? $this->SECOND_FIGHTER
            : $this->FIRST_FIGHTER;
    }

    public function getWinner()
    {
        return $this->FIRST_FIGHTER->health > $this->SECOND_FIGHTER->health
            ? $this->FIRST_FIGHTER
            : $this->SECOND_FIGHTER;
    }

    public function finish()
    {
        $this->consoleWindow->showMessages([
            "Game finished.",
            "{$this->getWinner()->name} WON."
        ]);
        $this->consoleWindow->separator();
    }

    /**
     * Method that will display the characters stats
     */
    public function printCharactersStats()
    {
        $this->consoleWindow->showTable(
            ["Stats", $this->FIRST_FIGHTER->name, $this->SECOND_FIGHTER->name],
            [
                ['Health', $this->FIRST_FIGHTER->health, $this->SECOND_FIGHTER->health],
                ['Strength', $this->FIRST_FIGHTER->strength, $this->SECOND_FIGHTER->strength],
                ['Speed', $this->FIRST_FIGHTER->speed, $this->SECOND_FIGHTER->speed],
                ['Defence', $this->FIRST_FIGHTER->defence, $this->SECOND_FIGHTER->defence],
                ['Luck', $this->FIRST_FIGHTER->luck, $this->SECOND_FIGHTER->luck],
            ]
        );
    }
}