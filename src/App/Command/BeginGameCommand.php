<?php

namespace App\Command;

use App\Factory\CharacterFactory;
use App\Model\Arena;

class BeginGameCommand
{
    public function __invoke()
    {

        Arena::getInstance()
            ->start(
                $hero = CharacterFactory::hero(),
                $beast = CharacterFactory::character()
            )
            ->beginFight()
            ->finish();
    }

}
