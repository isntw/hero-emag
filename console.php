<?php

$container = require __DIR__ . '/app/bootstrap.php';

$app = new Silly\Application();

// Silly will use PHP-DI for dependency injection based on type-hints
$app->useContainer($container, $injectWithTypeHint = true);

$app->command('game:start', 'App\Command\BeginGameCommand');

$app->run();
